//
//  ViewController.swift
//  BCIT-COMP3912-Week07-CoreLocation+MapKitRWApp
//
//  Created by Massimo Savino on 2016-10-31.
//  Copyright © 2016 Massimo Savino. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController {
    
    // MARK: Properties 
    
    @IBOutlet weak var mapView: MKMapView!

    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

}


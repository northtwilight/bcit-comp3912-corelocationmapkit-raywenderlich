//
//  Constants.swift
//  BCIT-COMP3912-Week07-CoreLocation+MapKitRWApp
//
//  Created by Massimo Savino on 2016-10-31.
//  Copyright © 2016 Massimo Savino. All rights reserved.
//

import Foundation

struct Constants {
    
    struct Basics {
        
        static let OK = "OK"
        static let Cancel = "Cancel"
    }
}
